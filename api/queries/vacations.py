from pydantic import BaseModel # NOTHING TO DO WITH THE DATABASE
from typing import Optional
from datetime import date
from queries.pool import pool

# class Thought(BaseModel):
#   private_thoughts: str
#   public_thoughts: str

class VacationIn(BaseModel):
  name: str
  from_date: date
  to_date: str
  thoughts: Optional[str]

class VacationOut(BaseModel):
  id: int
  name: str
  from_date: date
  to_date: date
  thoughts: Optional[str]

class VacationRepository:
  def create(self, vacation: VacationIn) -> VacationOut: 
    # Connect to the database
    with pool.connection() as conn:
      # Get a cursor; (something to run SQL with)
      with conn.cursor() as db:
        # Run our INSERT statement
        result = db.execute(
          """
          INSERT INTO vacations 
            (name, from_date, to_date, thoughts)
          VALUES 
            (%s, %s, %s, %s)
          RETURNING id;
          """,
          [
            vacation.name, 
            vacation.from_date, 
            vacation.to_date, 
            vacation.thoughts
          ]
        )
        id = result.fetchone()[0]
        # Return new data 
        old_data = vacation.dict()
        return VacationOut(id=id, **old_data)
    